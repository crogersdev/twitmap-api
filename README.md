# twitmap_api

python, postgres+postgis, aiohttp backend for twitmap.  now with containers!

eventually an app will be developed that will allow someone to report a sighting of a team's gear out and about.

examples:

"at zion's national park, i saw a utah state aggies hat"
"at disneyworld i saw a florida gators t-shirt"
"at ronald reagan national airport.  george mason, GW, and georgetown."

that location will be converted to lat/long coordinates and be explorable via a front end webapp with a map that allows the user to pan, zoom, search, filter, and sort where the fans are.
