#!/bin/bash

export PYTHONPATH=$PYTHONPATH:/twitmap/devops/src/db_setup
export PYTHONPATH=$PYTHONPATH:/twitmap/devops/src/py
export PYTHONPATH=$PYTHONPATH:/twitmap/src/tests/
export PYTHONPATH=$PYTHONPATH:/twitmap/src/db/
export PYTHONPATH=$PYTHONPATH:/twitmap/src/py/

source /twitmap/devops/twitmap_db.key