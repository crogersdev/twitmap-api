#!/usr/bin/python3
# -*- coding: utf-8 -*-

from TwitmapConfig import TwitmapConfig

cfg = TwitmapConfig()

DROP_TWITMAP_SCHEMA = 'DROP SCHEMA IF EXISTS %s CASCADE;' \
                        % (cfg.DEV_DBCONFIG.dbschema)

DROP_TWITMAP_SCHOOLS_TABLE = \
"""
DROP TABLE IF EXISTS %s.%s;
""" % (cfg.DEV_DBCONFIG.dbschema, cfg.DEV_DBCONFIG.schools_table)

DROP_TWITMAP_OBSERVANCES_TABLE = \
"""
DROP TABLE IF EXISTS %s.%s;
""" % (cfg.DEV_DBCONFIG.dbschema,
       cfg.DEV_DBCONFIG.observances_table)

DROP_TEST_OBSERVANCES_TABLE = \
"""
DROP TABLE IF EXISTS %s.%s;
""" % (cfg.DEV_DBCONFIG.dbschema,
       cfg.DEV_DBCONFIG.observances_test_data_table)
