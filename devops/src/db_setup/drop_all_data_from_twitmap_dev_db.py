#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : destroy_db.py
author: chris rogers
why   : run this script when you want to destroy all app related db entries
        e.g. schemas, tables, etc...
"""

# python modules
import sys

# pip modules
from psycopg2 import Error as PgError

# twitmap modules
import destroy_db_sql_statements as sql_cmds
from TwitmapConfig import TwitmapConfig
from TwitmapDbSession import TwitmapDbSession

CFG = TwitmapConfig()
DB_CFG = TwitmapDbSession()

try:
    DB_CFG.execute_raw_sql(sql_cmds.DROP_TWITMAP_OBSERVANCES_TABLE)
    print("Destroyed %s" % CFG.DEV_DBCONFIG.observances_table)
    DB_CFG.execute_raw_sql(sql_cmds.DROP_TWITMAP_SCHOOLS_TABLE)
    print("Destroyed %s" % CFG.DEV_DBCONFIG.schools_table)
    DB_CFG.execute_raw_sql(sql_cmds.DROP_TWITMAP_SCHEMA)
    print("Destroyed %s" % CFG.DEV_DBCONFIG.dbschema)
except PgError as error:
    print("Errors destroying database:")
    print('\t' + str(error.pgerror))
    print('\tWARNING: It is possible the database was not fully destroyed'
          'and is now in an undefined state.  Please try to destroy again.')
    raise BaseException('Errors destroying database.')

print("Done!")
