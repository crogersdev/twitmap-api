#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : put_schools_in_db.py
author: chris rogers
why   : csv reader for wiki-scraped csv file that has all 120 FBS teams
"""


# python modules
from csv import DictReader

# pip modules
from sqlalchemy import func

# twitmap modules
from TwitmapConfig import TwitmapConfig
from TwitmapDbBase import School
from TwitmapDbSession import TwitmapDbSession


CFG = TwitmapConfig()

session = TwitmapDbSession()

# TRICKY: Path to cfb_teams.csv is relative to who calls it.
# TODO(me): make this a configurable path or something less brittle
with open('/twitmap/devops/src/db_setup/cfb_teams.csv', 'r', newline='\n') as teams_file:
    READER = DictReader(teams_file)
    for count, row in enumerate(READER):
        if count == 0:
            continue

        print("Creating school geometry for %s at location (%s, %s)" %
              (row['school'], row['longitude'], row['latitude']))

        school_geom_point = func.ST_PointFromText('POINT(%s %s)' % (
            row['longitude'],
            row['latitude']
        ))

        if row['stadium_capacity'] == 'None':
            row['stadium_capacity'] = -1

        # TRICKY: from_json is a classmethod, which is a kooky alternate way
        #         of doing a ctor
        session.add(School.from_json(row))
