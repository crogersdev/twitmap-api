#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import namedtuple


class TwitmapConfig(object):
    def __init__(self):
        self.db_cfg = namedtuple('db_cfg', 'dbhost dbport dbname dbuser '
                                 'dbpasswd dbschema schools_table '
                                 'observances_table '
                                 'observances_test_data_table '
                                 'school_geom '
                                 'observance_geom')
        self.DEV_DBCONFIG = self.db_cfg(
            dbhost='localhost',
            dbport='5432',
            dbname='twitmap',
            dbuser='postgres',
            dbpasswd='postgres',
            dbschema='twitmap',
            schools_table='school',
            observances_test_data_table='observations_test_data',
            observances_table='observations',
            school_geom='school_geom',
            observance_geom='observance_geom')

        self.docker_cfg = namedtuple('docker_cfg',
                                     'container_name image_name image_tag '
                                     'volume_path volume_folder volumes '
                                     'volume_bindings ports port_bindings '
                                     'docker_sock restart_policy')
        self.DEV_DOCKER_CFG = \
            self.docker_cfg(
                container_name='twitmap_api_db',
                image_name='mdillon/postgis',
                image_tag='10-alpine',

                # used for filesystem prep
                volume_path='/data/twitmap_db',
                volume_folder='twitmap_postgis',

                # used to create the container volume bindings
                volumes=['/data/twitmap_postgis'],
                volume_bindings={
                    '/data/twitmap_postgis': {
                        'bind': '/data',
                        'mode': 'rw',
                    },
                },

                ports=[5432],
                port_bindings={5432: 5432},

                docker_sock='unix://var/run/docker.sock',

                restart_policy={
                    'MaximumRetryCount': 0,
                    'Name': 'always',
                },
            )

        self.app_cfg = namedtuple('app_cfg',
                                  'app_name api_container_name '
                                  'db_container_name app_log_filename')
        self.DEV_APPCONFIG = self.app_cfg(app_name='twitmap',
                                          api_container_name='twitmap_postgis',
                                          db_container_name='twitmap_api',
                                          app_log_filename='twitmap_app.log')

        self.webapp_cfg = namedtuple('webapp_cfg',
                                     'port')
        self.DEV_WEBAPPCONFIG = self.webapp_cfg(port=5001)
