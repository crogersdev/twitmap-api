#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : TwitmapDbBase.py
author: chris rogers
why   : base class for sql alchemy ORM database classes
        it's my intention to inherit from this class for each  -- dev, test,
        qa, prod, etc...  but we'll see how that turns out.

        we'll use this base class to create a db engine in sql alchemy
        which doesn't actually connect to the database, but abstracts the
        connection details
"""

# python modules

# pip modules
from geoalchemy2 import Geometry
from sqlalchemy import (
    Column,
    Float,
    ForeignKey,
    Integer,
    String,
)
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy_utils import (
    create_database,
    database_exists,
)

# twitmap modules
from TwitmapConfig import TwitmapConfig
from TwitmapDbSession import TwitmapDbSession

DB_CFG = TwitmapConfig()
Base = declarative_base()


class School(Base):
    """SQL Alchemy class for a School object."""

    __tablename__ = DB_CFG.DEV_DBCONFIG.schools_table

    id = Column(Integer, primary_key=True)
    school = Column(String)
    nicknames = Column('nicknames', postgresql.ARRAY(String))
    city = Column('city', String)
    state = Column('state', String)
    conference = Column('conference', String)
    latitude = Column('latitude', Float)
    longitude = Column('longitude', Float)
    population = Column('population', Integer)
    stadium_capacity = Column(Integer)
    school_geometry = Column(Geometry('POINT'))

    observations = relationship("Observation")
    test_observations = relationship("TestObservation")

    def __init__(self,
                 school,
                 nicknames,
                 city,
                 state,
                 conference,
                 latitude,
                 longitude,
                 population,
                 stadium_capacity,
                 school_geometry=None):
        """Create a School object."""
        self.school = school
        self.nicknames = nicknames
        self.city = city
        self.state = state
        self.conference = conference
        self.latitude = latitude
        self.longitude = longitude
        self.population = population
        self.stadium_capacity = stadium_capacity
        self.school_geometry = school_geometry

    @classmethod
    def from_json(cls, data):
        """Construct a School obj from a json blob."""
        return cls(**data)

    def to_json(self):
        """Convert a School obj to a json blob."""
        attr_names = [
            'id',
            'school',
            'nicknames',
            'city',
            'state',
            'conference',
            'latitude',
            'longitude',
            'population',
            'stadium_capacity',
            'school_geometry',
            'observations',
            'test_observations'
        ]

        attr_vals = [getattr(self, attr) for attr in attr_names]
        return dict(zip(attr_names, attr_vals))


class Observation(Base):
    """SQL Alchemy class for an Observation object."""

    __tablename__ = DB_CFG.DEV_DBCONFIG.observances_table

    observation_id = Column('observation_id', Integer, primary_key=True)
    school_id = Column(
        Integer,
        ForeignKey("school.id", ondelete="CASCADE"),
        nullable=False
    )
    observed_lat = Column(Float)
    observed_long = Column(Float)
    observation_geom = Column(Geometry('POINT'))

    school = relationship("School", back_populates="observations")

    def __init__(self,
                 school_id,
                 observed_lat,
                 observed_long,
                 observation_geom):
        """Construct an Observation obj from values."""
        self.school_id = school_id
        self.observed_lat = observed_lat
        self.observed_long = observed_long
        self.observation_geom = observation_geom

    @classmethod
    def from_json(cls, data):
        """Construct an Observation obj from json blob."""
        return cls(**data)

    def to_json(self):
        """Convert an Observation object to json blob."""
        attr_names = [
            'school_id',
            'observed_lat',
            'observed_long',
            'observation_geom',
        ]

        attr_vals = [getattr(self, attr) for attr in attr_names]
        return dict(zip(attr_names, attr_vals))


class TestObservation(Base):
    """SQL Alchemy class for a test Observation object."""

    __tablename__ = DB_CFG.DEV_DBCONFIG.observances_test_data_table

    observation_id = Column('observation_id', Integer, primary_key=True)
    school_id = Column(
        Integer,
        ForeignKey("school.id", ondelete="CASCADE"),
        nullable=False
    )
    observed_lat = Column(Float)
    observed_long = Column(Float)
    observation_geom = Column(Geometry('POINT'))

    school = relationship("School", back_populates="test_observations")

    def __init__(self,
                 school_id,
                 observed_lat,
                 observed_long,
                 observation_geom):
        """Construct an Observation obj from values."""
        self.school_id = school_id
        self.observed_lat = observed_lat
        self.observed_long = observed_long
        self.observation_geom = observation_geom

    @classmethod
    def from_json(cls, data):
        """Construct an Observation obj from json blob."""
        return cls(**data)

    def to_json(self):
        """Convert an Observation object to json blob."""
        attr_names = [
            'school_id',
            'observed_lat',
            'observed_long',
            'observation_geom',
        ]

        attr_vals = [getattr(self, attr) for attr in attr_names]
        return dict(zip(attr_names, attr_vals))


def create_all(db_session):
    """Create all db's."""
    if not database_exists(db_session.get_engine_url()):
        create_database(db_session.get_engine_url())
        db_session.create_postgis_extension()
        print("SQL Alchemy created database:", DB_CFG.DEV_DBCONFIG.dbname)

    try:
        resp = db_session.execute_raw_sql('SELECT PostGIS_full_version();')
        resp_list = resp.fetchall()
        if any([True if 'POSTGIS=' in x else False for x in resp_list]):
            print("Creating postgis extensions for the Twitmap DB...")
            db_session.create_postgis_extension()
            print("Done!")

        print("SQL Alchemy is creating DB objects for the Twitmap DB...")
        Base.metadata.create_all(db_session.get_engine())
        print("SQL Alchemy is done creating DB objects for the Twitmap DB!")

    except Exception as e:
        raise BaseException("Unable to create_all in SQL Alchemy:", str(e))


if __name__ == '__main__':
    db_session = TwitmapDbSession(env="dev")
    create_all(db_session)
