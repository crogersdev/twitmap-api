#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : TwitmapDbSession.py
author: chris rogers
why   : wrapper and getter for sql alchemy ORM db session
        iow, i don't want to keep typing the same nonsense over and over just
        to interact with the durn db
"""

# python modules
from functools import wraps

# pip modules
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# twitmap modules
from TwitmapConfig import TwitmapConfig

DB_CFG = TwitmapConfig()


def fails_gracefully(func):
    """Ensure the called function handles exceptions properly."""
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        """Call the wrapped function."""
        try:
            ret_val = func(self, *args, **kwargs)
        except Exception as e:
            raise "Problem found in {}: {}".format(
                str(__file__),
                str(e)
            )
        return ret_val
    return wrapper


class TwitmapDbSession(object):
    """Wraps and handles SQL Alchemy sessions and the like."""

    @fails_gracefully
    def __init__(self, env="dev"):
        """Create a db engine and set all public vars."""
        self._db_env = env
        if 'dev' in str(self._db_env).lower():
            self._engine_str = 'postgresql://%s:%s@%s:%s/%s' % (
                DB_CFG.DEV_DBCONFIG.dbuser,
                DB_CFG.DEV_DBCONFIG.dbpasswd,
                DB_CFG.DEV_DBCONFIG.dbhost,
                DB_CFG.DEV_DBCONFIG.dbport,
                DB_CFG.DEV_DBCONFIG.dbname
            )
            self._db_engine = create_engine(self._engine_str)
        else:
            raise "Unable to determine proper operating environment for DB."

        self._session_creator = sessionmaker(bind=self._db_engine)

        self._session = self._session_creator()

    @fails_gracefully
    def add(self, thing):
        """Add something."""
        self._session.add(thing)
        self._session.commit()

    @fails_gracefully
    def query(self, *args, **kw):
        """Query the db."""
        return self._session.query(*args, **kw)

    @fails_gracefully
    def execute_raw_sql(self, sql):
        """Execute raw sql.  this is a terrible idea."""
        with self._db_engine.connect() as conn:
            result_set = conn.execute(sql)

        return result_set

    @fails_gracefully
    def get_session(self):
        """Return the session object."""
        return self._session

    @fails_gracefully
    def get_engine(self):
        """Return the db engine."""
        # TODO(me): is this a good idea?
        return self._db_engine

    @fails_gracefully
    def get_engine_url(self):
        """Return the engine URL."""
        return self._db_engine.url

    @fails_gracefully
    def create_postgis_extension(self):
        """Create postgis extension for the db engine."""
        with self._db_engine.connect() as conn:
            conn.execute('CREATE EXTENSION postgis_topology CASCADE;')

    @fails_gracefully
    def bulk_insert(self, objects):
        """Perform bulk insert."""
        self._session.bulk_save_objects(objects)
        self._session.commit()
