#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : twitmap_impl.py
author: chris rogers
why   : rest endpoint implementations
"""

# python modules
from functools import wraps
import logging

# pip modules
from sqlalchemy import func
from sqlalchemy.exc import OperationalError

# twitmap modules
from DbConnectionError import DbConnectionError
from TwitmapConfig import TwitmapConfig
from TwitmapDbBase import (
    School,
    Observation,
    TestObservation,
)
from TwitmapDbSession import TwitmapDbSession


def needs_db_connection(func, *args, **kwargs):
    """Decorator to gracefully fail DB calls."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            results = func(*args, **kwargs)
        except OperationalError as oe:
            raise DbConnectionError(
                "Unable to execute {}.  Reason: {}"
                .format(func.__name__, str(oe))
            )
        else:
            return results
    return wrapper


class TwitmapImpl(object):
    """Interact with the database and show results.

    Define the way the rest endpoints interact with the
    database (through the SQLAlchemy and GeoAlchemy layers)
    and deliver the result sets back to the front end for rendering.
    """

    def __init__(self):
        """Create the Impl object."""
        self._APP_CFG = TwitmapConfig()

        logging.basicConfig(
            filename=self._APP_CFG.DEV_APPCONFIG.app_log_filename,
            level=logging.DEBUG
        )
        self._logger = logging.getLogger(__name__)
        self._logger.info("Created new instance of %s" % __name__)
        self.db_session = TwitmapDbSession(env="dev")

    @needs_db_connection
    def get_school_observations(self, school_name=None, polygon=None):
        """Retrieve all observations for a given school."""
        # TODO(crogers) consider storing the school name in the observation
        #                table.  humans look up by name, computers by id.
        #                don't want to have to make an extra db call for nothin
        self._logger.info("Get School Observations")

        if school_name and not polygon:
            school_id = self.db_session.query(School).filter(
                School.school == school_name
            ).all()[0].id

            return self.db_session.query(TestObservation).filter(
                TestObservation.school_id == school_id
            ).all()

        elif not school_name and polygon:
            return self.db_session.query(TestObservation).filter(
                func.ST_Contains(polygon, TestObservation.observation_geom)
            ).all()

        elif school_name and polygon:
            # TODO(crogers): get all observations of school_name in the polygon
            pass

        else:
            return []
