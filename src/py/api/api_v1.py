#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : api_v1.py
author: chris rogers
why   : rest endpoints (v1) for the twitmap middle-end
"""

# python modules
import json
import logging

# pip modules
import aiohttp_cors
from aiohttp import web

# twitmap modules
from common.ResourceBroker import ResourceBroker
from DbConnectionError import DbConnectionError
from TwitmapConfig import TwitmapConfig
from TwitmapDbBase import Observation
from TwitmapDbBase import TestObservation

API_CFG = TwitmapConfig()

routes = web.RouteTableDef()
logging.basicConfig(filename=API_CFG.DEV_APPCONFIG.app_log_filename,
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)


@routes.get('/ping')
async def ping(request):
    logger.info("ping request received")
    return web.Response(text='Pong!')


@routes.get('/observations/{school_name}')
async def get_school_observations(request):
    school_name = request.match_info['school_name']
    TwitmapImpl = ResourceBroker.get('TwitmapImpl')

    observations = await TwitmapImpl.get_school_observations(
        school_name=school_name
    )
    
    return web.Response(
        body=json.dumps({'data': [o.to_json() for o in observations]}),
        content_type="application/json",
    )


@routes.post('/observations')
async def get_all_observations_in_polygon(request):
    body = await request.json()
    polygon = body.get('box', None)
    if not polygon:
        return web.Response(
            text='Unable to parse requested polygon for observation retrieval',
            content_type='application/json',
        )

    # TODO(crogers): find out if this guy is inflated and destroyed with each
    #                endpoint call.  i'd really rather have it be created
    #                at the start of the app's life and the service created
    #                when it gets called for the first time, and from then
    #                on, it's only retrieved.
    TwitmapImpl = ResourceBroker.get('TwitmapImpl')

    try:
        import ipdb; ipdb.set_trace(context=11)
        observations = TwitmapImpl.get_school_observations(polygon=polygon)
    except DbConnectionError:
        return web.Response(
            body="{'data': 'Unable to talk to the database. " +
                 "Please consult web server logs.'}",
            content_type="application/json",
        )
    """
    observances_geojson = {}

    observances_features = [{"type": "Feature", "geometry": { "type": "Point", "coordinates": ["{0:.2f}".format(o.observed_long), "{0:.2f}".format(o.obesrved_lat)] }}]

    json_blob = {'num_observations': len(observations), 'data': observances_geojson}

    inner = [{'lng': o.observed_long, 'lat': o.observed_lat, 'school': o.school.school} for o in observations]

    # example geojson:

    var stores = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                -77.034084142948,
                38.909671288923
                ]
            },
            "properties": {
                "phoneFormatted": "(202) 234-7336",
                "phone": "2022347336",
                "address": "1471 P St NW",
                "city": "Washington DC",
                "country": "United States",
                "crossStreet": "at 15th St NW",
                "postalCode": "20005",
                "state": "D.C."
            }
            },
            {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                -77.049766,
                38.900772
                ]
            },
            "properties": {
                "phoneFormatted": "(202) 507-8357",
                "phone": "2025078357",
                "address": "2221 I St NW",
                ...
            }
        }
    ]
    """
    json_blob = ""
    return web.Response(
        body=json.dumps(json_blob),
        content_type="application/json",
    )


def create_twitmap_app(loop):
    twitmap_app = web.Application(loop=loop)

    # TRICKY: Please remove me when this is no longer in dev.
    cors = aiohttp_cors.setup(twitmap_app)
    for r in routes:
        resource = cors.add(twitmap_app.router.add_resource(r.path))
        cors.add(
            resource.add_route(r.method, r.handler),
            {'*': aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers=("X-Custom-Server-Header",),
                allow_headers=("X-Requested-With", "Content-Type"),
                max_age=3600)
             }
        )
    twitmap_app.router.add_routes(routes)
    return twitmap_app
