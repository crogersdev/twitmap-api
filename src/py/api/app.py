#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : app.py
author: chris rogers
why   : starts the app
"""

# python modules
import asyncio
import logging

# pip modules
from aiohttp import web

# twitmap modules
from api.api_v1 import (
    create_twitmap_app
)
from api.TwitmapImpl import TwitmapImpl
from common.ResourceBroker import ResourceBroker
from TwitmapConfig import TwitmapConfig


WEBAPP_CFG = TwitmapConfig()
logging.basicConfig(filename=WEBAPP_CFG.DEV_APPCONFIG.app_log_filename,
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

ResourceBroker.register('TwitmapImpl', TwitmapImpl)

app = create_twitmap_app(loop=asyncio.get_event_loop())
logger.info("Running web app")
web.run_app(app, port=WEBAPP_CFG.DEV_WEBAPPCONFIG.port)
