#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : ResourceBroker.py
author: zoran isailovsky (http://code.activestate.com/recipes/413268/)
        chris rogers (some changes, but mostly zoran's stuff)
why   : implements loose coupling between modules
        the specific use case is when i don't want the api_v1.py file
        to have to be aware of how to handle the business logic of
        facilitating/handling API requests.

        i anticipate having a wide array of routes, each one requiring a/many
        class/classes as it makes sense for cognitive load.  i also wish to
        keep things small and independent and modularself.

        so i could just import whatever class i need into api_v1.py, but then
        i've introduced a dependency and every time i change the class i'm
        importing, i have to (potentially) make changes to the code that uses
        it.

        as an aside, from my C++ days, i recall designing interfaces (read:
        function signatures) that were supposed to be backwards compatible.  i
        i still don't quite know what to make of "breaking changes" outside of
        missing or newly required args to class new()'s or methods or free
        floating functions.

        anyway.  this is supposed to help alleviate things.  it feels like it's
        a delegation of resource instance creation to something that will live
        at the aiohttp app level rather than at the route definition level.
        the route definition level will come to life and die with each
        REST call.
"""

# python modules

# pip modules

# twitmap modules
from common.Singleton import Singleton


class ResourceBroker(Singleton):
    """Broker the creation of resources.

    Has a list of resources in the self.resources dict and
    """

    def __init__(self, allow_replace=False):
        """Construct an instance, initialize dict of resources."""
        self.resources = {}
        self.allow_replace = allow_replace

    def register(self, resource_name, resource, *args, **kwargs):
        """Register, or add, a resource to the broker.

        param: allow_replace
        Boolean for allowing the replacement of the instance.
        """
        if not self.allow_replace:
            assert resource_name not in self.resources, \
                'Resource already registered: %r' % resource

        if callable(resource):
            def call():
                """Return callable resource."""
                return resource(*args, **kwargs)
        else:
            def call():
                """Return resource."""
                return resource

        self.resources[resource_name] = call

    def get(self, resource_name):
        """Retrieve a resource."""
        return self.__getitem__(resource_name)

    def __getitem__(self, resource_name):
        """Retrieve a resource."""
        try:
            resource = self.resources[resource_name]
        except KeyError:
            raise KeyError('ResourceBroker: Unknown resource named %r'
                           % resource_name)

        return resource()


ResourceBroker = ResourceBroker()
