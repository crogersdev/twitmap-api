#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : api_v1.py
author: chris rogers
        Reference: https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
why   : to make the ResourceBroker a singleton
"""

# python modules

# pip modules

# twitmap modules


class SingletonMetaClass(type):
    _instance = None

    def __call__(cls, *args, **kw):
        """Override call method.

        If class is in the instances, return itself.  Otherwise, create a new
        one and then return it.
        """
        if not cls._instance:
            cls._instance = \
                super(SingletonMetaClass, cls).__call__(*args, **kw)
        return cls._instance


class Singleton(object, metaclass=SingletonMetaClass):
    pass
