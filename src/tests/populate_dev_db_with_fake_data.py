#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
file  : populate_dev_db_with_fake_data.py
author: chris rogers
why   : puts data in the observances table so we have some fake data
        to work with.
"""

# python modules
from random import (
    choice,
    seed,
    uniform,
)
import time

# pip modules
from sqlalchemy import func

# twitmap modules
from TwitmapDbBase import (
    School,
    TestObservation
)
from TwitmapDbSession import TwitmapDbSession

# http://en.wikipedia.org/wiki/Extreme_points_of_the_United_States#Westernmost
TOP = 49.3457868      # north lat
LEFT = -124.7844079   # west long
RIGHT = -66.9513812   # east long
BOTTOM = 24.7433195   # south lat

session = TwitmapDbSession(env="dev")

NUM_FAKE_OBSERVANCES = 5000

seed(time.clock())

print("Inserting %s records into the observances test data table" %
      NUM_FAKE_OBSERVANCES)

for i in range(0, NUM_FAKE_OBSERVANCES):
    lat = uniform(BOTTOM, TOP)
    lng = uniform(LEFT, RIGHT)

    which_school = choice(range(0, 3))

    if which_school == 0:
        s = 'Utah State'
    elif which_school == 1:
        s = 'Virginia Tech'
    elif which_school == 2:
        s = 'Rutgers'
    else:
        s = 'Virginia'

    school = session.query(School).filter(School.school == s).all()[0]
    print("inserting %s seen at (lat: %02f, long: %02f)" % (s, lat, lng))

    test_observation_geom = func.ST_PointFromText(
        'POINT({} {})'.format(lng, lat)
    )

    session.add(TestObservation(
        school_id=school.id,
        observed_lat=lat,
        observed_long=lng,
        observation_geom=test_observation_geom
    ))

print("Done!")
